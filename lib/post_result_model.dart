import 'dart:convert';

import 'package:http/http.dart' as http;

// Buat Class

class PostResult {
  // Buat variable data
  String id;
  String name;
  String job;
  String created;

  // Buat Constructor
  PostResult({this.id, this.name, this.job, this.created});

  // Mapping Json Object
  factory PostResult.createPostResult(Map<String, dynamic> object) {
    return PostResult(
        id: object['id'],
        name: object['name'],
        job: object['job'],
        created: object['createdAt']);
  }

  //Menghubungkan App ke API
  static Future<PostResult> connectToAPI(String name, String job) async {
    String apiURL = "https://reqres.in/api/users";

    var apiResult = await http.post(apiURL, body: {"name": name, "job": job});
    var jsonObject = json.decode(apiResult.body);

    return PostResult.createPostResult(jsonObject);
    
  }
}
